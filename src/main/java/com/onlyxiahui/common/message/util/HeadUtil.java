package com.onlyxiahui.common.message.util;

import java.util.HashSet;
import java.util.Set;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.common.message.base.ResultHead;
import com.onlyxiahui.framework.action.dispatcher.general.constant.HeadProperty;

/**
 * 
 * <br>
 * Date 2019-12-03 17:18:24<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class HeadUtil {

	private static Set<String> headProperties = new HashSet<>();

	public static ResultHead toHead(JSONObject jo) {
		ResultHead head = new ResultHead();
		if (null != jo) {
			for (HeadProperty key : HeadProperty.values()) {
				put(head, key.getName(), jo.get(key.getName()));
			}
			for (String key : headProperties) {
				put(head, key, jo.get(key));
			}
		}
		head.setTimestamp(System.currentTimeMillis());
		return head;
	}

	public static void put(ResultHead head, String key, Object value) {
		if (null != head && null != key && null != value) {
			head.put(key, value);
		}
	}

	public static Set<String> getHeadProperties() {
		return headProperties;
	}

	public static void addHeadProperty(String property) {
		headProperties.add(property);
	}
}
