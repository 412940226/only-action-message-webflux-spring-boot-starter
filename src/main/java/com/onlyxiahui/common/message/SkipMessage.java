package com.onlyxiahui.common.message;

/**
 * 
 * 不进行拦截的消息<br>
 * Date 2019-09-06 14:26:20<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public interface SkipMessage {

}
