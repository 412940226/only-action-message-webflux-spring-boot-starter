package com.onlyxiahui.framework.action.dispatcher.general.extend.impl;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.HandlerResult;
import org.springframework.web.server.ServerWebExchange;

import com.onlyxiahui.extend.spring.webflux.dispatcher.result.handler.ResponseBodyHandler;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;
import com.onlyxiahui.framework.action.dispatcher.general.constant.ActionConstant;
import com.onlyxiahui.framework.action.dispatcher.general.extend.impl.handler.ResultDataHandler;

/**
 * Description <br>
 * Date 2020-06-08 16:31:26<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Component
public class ResponseBodyHandlerImpl implements ResponseBodyHandler {

	ResultDataHandler h = new ResultDataHandler();

	@Override
	public Object handle(ServerWebExchange exchange, HandlerResult result) {
		Object data = result.getReturnValue();
		ArgumentBox argumentBox = exchange.getAttribute(ActionConstant.ARGUMENT_BOX);
		String body = exchange.getAttribute(ActionConstant.REQUEST_BODY_CACHE);
		return h.result(data, body, argumentBox);
	}
}
