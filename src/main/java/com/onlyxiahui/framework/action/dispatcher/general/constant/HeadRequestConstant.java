package com.onlyxiahui.framework.action.dispatcher.general.constant;

/**
 * 
 * <br>
 * Date 2019-12-03 16:17:57<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class HeadRequestConstant {

	/**
	 * 请求来源系统
	 */
	public static String SOURCE = "source";
	/**
	 * 请求令牌
	 */
	public static String TOKEN = "token";

}
