package com.onlyxiahui.framework.action.dispatcher.general.extend.impl;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.common.message.base.HeadImpl;
import com.onlyxiahui.common.message.base.ResultHead;
import com.onlyxiahui.common.message.node.Head;
import com.onlyxiahui.common.message.util.HeadUtil;
import com.onlyxiahui.framework.action.dispatcher.ActionDispatcher;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionMessage;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;
import com.onlyxiahui.framework.action.dispatcher.general.constant.DataConstant;
import com.onlyxiahui.framework.action.dispatcher.general.extend.ActionMessageResolver;
import com.onlyxiahui.framework.action.dispatcher.general.util.ActionDispatcherJsonUtil;

/**
 * Description <br>
 * Date 2020-06-08 15:31:02<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Component
public class ActionMessageResolverImpl implements ActionMessageResolver {

	@Override
	public ActionMessage resolver(ActionDispatcher actionDispatcher, String path, Object data, ArgumentBox argumentBox) {
		String message = (data instanceof String) ? data.toString() : "";
		if (ActionDispatcherJsonUtil.maybeJson(message)) {
			JSONObject jo = JSONObject.parseObject(message);

			argumentBox.put(JSONObject.class, jo);

			boolean hasHead = jo.containsKey(DataConstant.HEAD) && jo.get(DataConstant.HEAD) instanceof JSONObject;
			if (hasHead) {
				JSONObject headElement = jo.getJSONObject(DataConstant.HEAD);
				HeadImpl head = headElement.toJavaObject(HeadImpl.class);
				ResultHead resultHead = HeadUtil.toHead(headElement);

				argumentBox.put(Head.class, head);
				argumentBox.put(HeadImpl.class, head);
				argumentBox.put(ResultHead.class, resultHead);

				String classCode = (head.getAction() == null) ? "" : head.getAction();
				String methodCode = head.getMethod();
				if (null != methodCode && !methodCode.isEmpty()) {
					path = actionDispatcher.getActionRegistry().getPath(classCode, methodCode);
				}
			}
		}
		ActionMessage am = new ActionMessageImpl();
		am.setMessage(data);
		am.setAction(path == null ? "" : path);
		return am;
	}

	class ActionMessageImpl implements ActionMessage {

		Object message;
		String action;

		@Override
		public void setMessage(Object message) {
			this.message = message;
		}

		@Override
		public Object getMessage() {
			return this.message;
		}

		@Override
		public String getAction() {
			return this.action;
		}

		@Override
		public void setAction(String action) {
			this.action = action;
		}
	}
}
