package com.onlyxiahui.framework.action.dispatcher.general.extend.impl;

import org.springframework.stereotype.Component;

import com.onlyxiahui.framework.action.dispatcher.ActionContext;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionRequest;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;
import com.onlyxiahui.framework.action.dispatcher.extend.ResultHandler;
import com.onlyxiahui.framework.action.dispatcher.general.extend.impl.handler.ResultDataHandler;

/**
 * Description <br>
 * Date 2020-06-08 16:22:03<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Component
public class ResultHandlerImpl implements ResultHandler {

	ResultDataHandler h = new ResultDataHandler();

	@Override
	public Object handle(ActionContext actionContext, Object data, ActionRequest request, ArgumentBox argumentBox) {
		return h.result(data, request.getData(), argumentBox);
	}
}
